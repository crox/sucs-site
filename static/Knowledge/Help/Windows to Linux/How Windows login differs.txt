



The Linux login is similar to Windows when you first encounter it, and because of this you may think its nothing special, as I did. Well, you&#39;d be wrong (as I was).

<h2>Get to a Machine!</h2>
There are several ways to do this, the easiest being to simply go to 
the room, find a PC and log in using your username and password &acirc;�� if you 
cant remember these, find a friendly admin or exec member, and tell 
them, they will more than likely be pleased to help you out because it 
means they get the opportunity to laugh at your forgetfulness :-D

<h2>The Joy of SSH...</h2>
However, the more cunning fox will use a facility known as <a href="http://en.wikipedia.org/wiki/SSH" title="Technical Info!">SSH</a>.
<br />What this is, technically, is a Secure SHell. i.e. a means with which 
to log into our server from anywhere in the world that is connected to 
the Internet, securely (So people can&#39;t go stealing your passwords etc).
<br />This is the really one of the main reasons why I started converting to Linux.<br />

<ul>
<li>For a start its secure, which is incredibly important these days as more spam and <a href="http://en.wikipedia.org/wiki/Adware" title="Technical Info!">adware</a> applications make use of our communications to glean email addresses and information.</li>
<li>You can log in from <span style="font-style: italic">anywhere</span> with an Internet connection. This includes dialup, though as ever it will be slower!</li>
<li>You can &#39;bounce&#39; around machines, I&#39;ll go into this later.</li>
<li>It is a means by which many other applications can communicate securely (e.g., Email, web tunnelling, <a href="http://en.wikipedia.org/wiki/Port_forwarding" title="Technical Info!">port forwarding</a> etc)</li>
</ul>
<p>N.B. this is a <span style="font-weight: bold">text based</span> service, so don&#39;t expect fireworks. However there are some cunning things that can be done with it, and a graphical approach is described a bit further down (<span style="font-style: italic">VNC and DAV</span>). I will get to 
these shortly (promise), but for now let&#39;s look at actually logging in...</p>

<h3>Windows:</h3>
<p>Windows does not have an inbuilt SSH package, but the easiest way is 
to use a utility called PuTTY. This can be found <a href="http://the.earth.li/~sgtatham/putty/latest/x86/putty.exe" title="PuTTY Download">here</a>. So, once you have that, simply run it and visit our <a href="../SUCS%20Services/Logging%20in%20remotely/Configuring%20PuTTY" title="PuTTY Config and Usage">wonderful guide</a> on how to configure it (its very easy, I managed it so anyone can).<br />
PuTTY acts as a terminal window emulator, that is, it looks like a Shell that you would use on a Linux PC.<br />
From here you can do anything that you could do if you were using a shell, apart from one of the &#39;party tricks&#39; that is X-Forwarding</p>

<h3>Linux:</h3>
<p>Easy, open a shell and type:
</p><pre class="console">ssh [username]@sucs.org</pre>
(Don&#39;t include the [ ] !)
<p>This assumes that you have SSH installed, and you should do unless you 
are on some weird embedded system (in which case you need to get 
yourself on a forum and ask, or <a href="http://www.google.co.uk" title="Google! Use it! Lots!!">Google</a> for it.)</p>

<h2>Done that?</h2>
OK! So now you can logon! If you wish to log on to a different machine then once you have logged on as above, simply type:
<pre class="console">ssh [computername]</pre>
<p>You will be prompted for your password (unless you set up 
<a href="../SUCS%20Services/Logging%20in%20remotely/Using%20Authorisation%20Keys%20to%20log%20in" title="Setting up authorisation keys">autorization keys</a>)</p>
<p>Example:</p>
<pre class="console">ssh platinum</pre>

<p>will log you onto platinum via our server; silver.</p>

<h3>The exciting bits!!!</h3>
<ol><li><a href="../SUCS%20Services/Using%20Milliways">Milliways</a> - The SUCS chat system; OK, so this isnt overly exciting but still...</li>
<ol><li>Ensure the prompt says: [username]@silver [something]&gt; The &#39;@silver&#39; is important as silver is the name of our server... </li>
<li>Type: &#39;mw&#39; and follow the instructions! <br /></li><li>Bingo, you now get to converse with all the regulars in the room; students, postgrads and Old-Timers who have gone out to the Real World (eek!).</li>
</ol>
<li>LINUX ONLY! OK, Here my friends, is where Linux is the proverbial bees knees, the cats pyjamas etc...</li>
<ol>
<li>You can run any of our applications available at SUCS on your home 
linux machine by using a service known as X-Forwarding. <br />THIS IS COOL! Honestly, if there was a defining list of where Linux out-does Windows, this would be on there.<br />
This simply forwards graphical information to your computer and acts 
(mostly) as if you are sitting at the machine in the SUCS room. It also works the other way round, if you were at the SUCS room and logged into a Linux machine at home then you could forward applications from home to SUCS.</li>
<li>To do this: before logging on, add in the switch -Y:
<pre class="console">ssh -Y [username]@sucs.org</pre></li>
<li>Voila, done.<br />
N.B. This may not work wholly successfully for some applications, but 
mostly its all good. If you want to have a go at making X forwarding possible, check out Cygwin, which is a Linux emulator for Windows.<br /></li>
</ol>
</ol>
<p>OK, so here we see the use of SSH. Like I mentioned this is only TEXT.<br />If you want a sparkly Graphical remote interface, then Linux provides...</p>
<h3>VNC and DAV</h3>
<a href="http://en.wikipedia.org/wiki/VNC" title="Technical Info!">VNC</a>, or Virtual Network Computing is basically about making your computer&#39;s desktop available across a network.<br />
This is good, it means you can be sitting at home and act as if you are sitting at a machine in SUCS; <span style="font-weight: bold; font-style: italic">graphically</span>!<br />
Ok, so this technology isnt restricted to Linux, and is in fact well supported under windows.<br />
But the point is, Linux need not be scary and Text based, it has a shiny soft coat called X.
<p>(<span style="font-weight: bold">Aside</span>: X is the <span style="font-weight: bold">Graphical User Interface (GUI)</span> that sits on top of the scary looking text-based gubbins. It gives you cursors, pictures, a mouse pointer and other sparkly things :D, this is a slight simplification but you get the idea.)</p>
<p>Right, so first thing&#39;s first, get yourself a VNC client.<br />The easiest way to do this is to simply <a href="../../../Tools/Desktop%20on%20Demand" title="VNC in your browser!">use the Java client</a>. For this to work you will need Java enabled/installed (see <a href="http://java.com/en/download/windows_xpi.jsp" title="Java download (Win XP)">here</a>)<br /><br />OK, failing that we have a wonderful tool available freely in windows called RealVNC, which can be downloaded <a href="http://www.realvnc.com/download-free.html" title="RealVNC Download">here</a> (Just fill in the details and hit proceed).</p>
<p>Once installed, you will have a server running on your machine as well as the viewer to access other machines. You don&#39;t need the server unless you want to be able to access your machine from elsewhere (which is handy), however you should set a password on it at the start incase you ever decide to use it.</p>
<p>(<span style="font-weight: bold">Aside</span>: To set this, you should be presented with a dialog when you first install. Go to &#39;Authentication&#39; and click the &#39;Set Password&#39; button.)</p>
Ok, so once you have succesfully installed:
<ol>
<li>Go to programs and find VNCViewer under RealVNC</li>
<li>When it runs you will be presented with a dialog asking for the hostname</li>
<li>Enter: <span style="font-weight: bold">sucs.org:1 </span>(I will explain this in a moment)</li>
<li>You should now be presented with an 800x600 size window containing a rather nasty grey thing</li>
<li>This will quickly change to a nice pretty interface, similar to the one in the room (Just a login screen for those of you who havent been to the room)</li>
<li>Enter your username and password into the fields, and off you go! <br /></li>
<li>You should now have a full desktop in a window, fully controllable as if you were there!</li>
</ol>
<p><span style="font-weight: bold">Some notes:</span><br />
The line we entered in 3. can be explained as follows, for you curious folk:<br /></p><ul><li><span style="font-weight: bold">sucs.org</span> is (as you probably guessed) the name of our server in SUCS, or the <span style="font-weight: bold">hostname</span></li></ul><p>&nbsp;</p><ul><li><span style="font-weight: bold">:1</span> is the screen number we are connecting to. It is what tells the server that we are connecting to a <span style="font-weight: bold">virtual desktop</span>. What this means is that we are not actually controlling a machine in the room directly, we are just running a <span style="font-weight: bold">session</span> on it. Very similar to when you log in using SSH, this is simply an X session.</li></ul><p>
The advantage of Linux over Windows in this respect is that you <span style="font-style: italic">can</span> have sessions, and easily. Lots of people can all be running X and SSH sessions on one machine, along with someone actually sitting there using it. Obviously this means the computer&#39;s resources are stretched a little, but this is negligable these days. (Especially if someone decides to donate some high powered processors/ram etc to us :-D )<br /></p><h3>DAV</h3>I shan&#39;t explain how to use <a href="http://en.wikipedia.org/wiki/WebDAV" title="Technical Info!">DAV</a> here as its covered in this page <a href="../SUCS%20Services/Using%20WebDAV" title="Using WebDAV">&#39;Using WebDAV&#39;</a> rather, I shall explain briefly why you would use it.<br /><br />Basically, DAV allows you to read and write to your home directory (As if it were a folder on your machine) over the internet.<br /><br />Its incredibly easy to use and if you <span style="font-style: italic">really</span> don&#39;t want to get into Linux (more fool you), or need a quick access to your files from anywhere then use DAV.<br /><br /><h3>Summary</h3><br /><br />If you want to get into Linux in a big way, log in using SSH and VNC. This will allow you to get used to the environment in a VNC window without having to be locked into a Linux machine (which, by the way, you don&#39;t have to do; see <a href="win2lin.convert" title="Installing and Using Linux">Converting</a>). You will use SSH to get to grips with the command line and if it all gets scary, you can use the VNC window as a backup.<br /><br /><a href="win2lin.convert" title="Installing and Using Linux">&nbsp;</a>If you want to be able to use Linux competently but don&#39;t like the text base that is used, go for VNC, but remember the forfeit here is that it can be slooow over lower band widths.<br /><br />And, if you are not feeling up to the challenge and just want to be able to access you files; DAV will help.<br /><br />More on <a href="win2lin.fileaccess">File Access</a>.<br /><br />Next Page: <a href="win2lin.email" title="Setting up Email in Linux">EMail Setup</a><br /><br /><sub>Questions/suggestions for modification - <a href="mailto:stringfellow@sucs.org" title="Email Steve P">stringfellow@sucs.org</a></sub><br />