<h1>Constitution of the SU Computer Society</h1>
<h2>0  Introduction</h2>
<p> This the constitution of the Swansea University Computer Society
(S.U.C.S.) henceforth referred to as the Society. It sets out the rules
governing the society and its objectives.  </p>
<h2>1  Aims And Objectives Of The Society</h2>
<table border="0">
<tbody>
<tr>
<td>1.1</td>
<td>To help further the education of the members.</td>
</tr>
<tr>
<td>1.2</td>
<td>To do anything the committee sees fit to enable 1.1.</td>
</tr>
<tr>
<td>1.3</td>
<td>To have fun</td>
</tr>
</tbody>
</table>
<h2>2	Membership</h2>
<table border="0">
<tbody>
<tr valign="top">
<td>2.1</td>
<td>Membership is divided into:<br />
<table border="0">
<tbody>
<tr>
<td>(i)</td>
<td>Honorary members</td>
</tr>
<tr>
<td>(ii)</td>
<td>Student members</td>
</tr>
<tr>
<td>(iii)</td>
<td>Alumnus members</td>
</tr>
</tbody>
</table>
The term <em>'members'</em> shall denote (i), (ii) and (iii) unless otherwise indicated.</td>
</tr>
<tr>
<th>2.2</th><th align="left">Honorary Membership</th>
</tr>
<tr valign="top">
<td>2.2.1</td>
<td>Any person can become an honorary member, at the discretion of the committee.</td>
</tr>
<tr valign="top">
<td>2.2.2</td>
<td>Once a person has become an honorary member they retain that membership unless it is terminated by the committee.</td>
</tr>
<tr valign="top">
<td>2.2.3</td>
<td>A person whose honorary membership has been terminated under 2.2.2 must be (if possible) informed in writing by the committee.</td>
</tr>
<tr valign="top">
<td>2.2.4</td>
<td>Retiring committee members may become honorary members of the society, in accordance with 2.2.1.
<style type="text/css"><!--
p { margin-bottom: 0.25cm; line-height: 120%; }
--></style>
</td>
</tr>
<tr valign="top">
<td>2.2.5</td>
<td>The General Secretary must maintain a list of the honorary members of the society.</td>
</tr>
<tr>
<th>2.3</th><th align="left">Student Membership</th>
</tr>
<tr valign="top">
<td>2.3.1</td>
<td>This society is open to all members of Swansea University Students' Union.</td>
</tr>
<tr valign="top">
<td>2.3.2</td>
<td>Student members have voting rights, to elect an executive (yearly) and to vote at a members' meeting.</td>
</tr>
<tr valign="top">
<td>2.3.3</td>
<td>Student membership is conditional upon the payment of the membership fee.</td>
</tr>
<tr valign="top">
<td>2.3.4</td>
<td>A student member's membership is terminated upon unanimous (allowing for abstentions) vote of the committee, for disciplinary reasons.</td>
</tr>
<tr valign="top">
<td>2.3.5</td>
<td>The General Secretary must maintain a list of the student members of the society.</td>
</tr>
<tr valign="top">
<td>2.3.6</td>
<td>When a student member's status changes with regard to Swansea University Student Union membership, this change must be informed to the committee directly by the member.</td>
</tr>
<tr valign="top">
<td>2.3.7</td>
<td>If a student member's status changes without notification to the committee, the membership will default to honorary.</td>
</tr>
<tr valign="top">
<td>2.3.8</td>
<td>The change of status from student to honorary membership by the committee must be notified in writing to the member before the change occurs.</td>
</tr>
<tr valign="top">
<td>2.3.9</td>
<td>If a student member becomes an honorary member due to a change of status per sections 2.3.6 - 2.3.8 this honorary membership will end at the time the student membership would have ended, unless the committee decide otherwise</td>
</tr>
<tr valign="top">
<td>2.3.10</td>
<td>For the purposes of nominations only, any student member whose status changes during the period of nomination for elections shall be treated as having the status they had when nominations opened. This status shall expire at the close of nominations.</td>
</tr>
<tr valign="top">
<td>2.3.11</td>
<td>For the purposes of voting only, any student member whose status changes during the period of voting shall be treated as having the status they had when the period of voting began. This status shall expire when the period of voting ends.</td>
</tr>
<tr>
<th>2.4</th><th align="left">Alumnus Membership</th>
</tr>
<tr valign="top">
<td>2.4.1</td>
<td>Alumnus membership is open to any person who has, at some time, been a member of Swansea University Students' Union, at the discretion of the committee.</td>
</tr>
<tr valign="top">
<td>2.4.2</td>
<td>Alumnus membership is conditional upon the payment of the alumnus membership fee.</td>
</tr>
<tr valign="top">
<td>2.4.3</td>
<td>An alumnus member's membership is terminated upon unanimous
(allowing for abstentions) vote of the committee, for disciplinary reasons.</td>
</tr>
<tr valign="top">
<td>2.4.4</td>
<td>A person whose alumnus membership has been terminated under
2.4.3 must be (if possible) informed in writing by the committee.</td>
</tr>
<tr valign="top">
<td>2.4.5</td>
<td>The General Secretary must maintain a list of the alumnus
members of the society.
</td>
</tr>
<tr valign="top">
<td>2.5<br /></td>
<td>All members are subject to the <a href="Conditions">Terms and Conditions</a>.<br /></td>
</tr>
</tbody>
</table>
<h2>3	Meetings</h2>
<table border="0">
<tbody>
<tr>
<th>3.1</th><th align="left">Members' meetings</th>
</tr>
<tr valign="top">
<td>3.1.1</td>
<td>There shall be at least 1 members' meeting a year.</td>
</tr>
<tr valign="top">
<td>3.1.2</td>
<td>A members' meeting can be called at any time by the General Secretary or the President by giving at least 2 days' notice for an Emergency meeting* or at least 7 days' notice for any other meeting.</td>
</tr>
<tr valign="top">
<td>&nbsp;</td>
<td><em>* An Emergency meeting is a members' meeting which is called due to matters arising (and may have some urgency) and at which only one subject is on the agendum.</em></td>
</tr>
<tr valign="top">
<td>3.1.3</td>
<td>The General Secretary MUST call a members' meeting to be held 7 days (or nearest available day thereafter) after receipt of a petition from a member. Such a petition can not be electronic and MUST be signed by at least 1/3 of the student membership.</td>
</tr>
<tr valign="top">
<td>3.1.4</td>
<td>A meeting is quorate if 10% or more of the student members are present</td>
</tr>
<tr valign="top">
<td>3.1.5</td>
<td>The agenda for a meeting must be published no later than the time-scales defined in section 3.1.2 for the notice of said meeting.</td>
</tr>
<tr>
<th>3.2</th><th align="left">Committee meetings</th>
</tr>
<tr valign="top">
<td>3.2.1</td>
<td>There shall be at least 1 committee meeting each term.</td>
</tr>
<tr valign="top">
<td>3.2.2</td>
<td>A committee meeting can be called at any time by any committee member provided a reasonable attempt is made to inform committee members of the meeting.</td>
</tr>
</tbody>
</table>
<h2>4	Executive Officers Of The Society</h2>
<table border="0">
<tbody>
<tr valign="top">
<td>4.1</td>
<td>There shall be a President of the Society, who shall be the senior officer.</td>
</tr>
<tr valign="top">
<td>&nbsp;</td>
<td>The President shall be a deputy Treasurer and can deputise for, have, and be entitled to exercise all the duties and powers of the Treasurer in their absence.</td>
</tr>
<tr valign="top">
<td>&nbsp;</td>
<td>The President shall be a deputy General Secretary and can deputise for, have, and be entitled to exercise all the duties and powers of the General Secretary in their absence.</td>
</tr>
<tr valign="top">
<td>&nbsp;</td>
<td>The President shall be a deputy to all other committee members and can deputise for the absent committee member in his/her absence.</td>
</tr>
<tr valign="top">
<td>4.2</td>
<td>There shall be a Treasurer of the Society, who shall be in charge of, and be responsible for the financial administration of the society. The Treasurer shall have a duty to present the financial accounts.</td>
</tr>
<tr valign="top">
<td>&nbsp;</td>
<td>The Treasurer shall be the senior deputy President and can deputise for, have, and be entitled to exercise all the duties and powers of the President in their absence.</td>
</tr>
<tr valign="top">
<td>&nbsp;</td>
<td>The Treasurer shall be a deputy General Secretary and can deputise for, have, and be entitled to exercise all the duties and powers of the General Secretary in their absence.</td>
</tr>
<tr valign="top">
<td>4.3</td>
<td>There shall be a General Secretary of the Society, who shall be in charge of, and be responsible for the administration of the society. They are also responsible for the receipt of any petition from the members and should make efforts to represent the members' opinions on committee. They are responsible for maintaining the membership lists.</td>
</tr>
<tr valign="top">
<td>&nbsp;</td>
<td>The General Secretary shall be a deputy President and can deputise for, have, and be entitled to exercise all the duties and powers of the President in their absence.</td>
</tr>
<tr valign="top">
<td>&nbsp;</td>
<td>The General Secretary shall be a deputy Treasurer and can deputise for, have, and be entitled to exercise all the duties and powers of the Treasurer in their absence.</td>
</tr>
<tr valign="top">
<td>4.4</td>
<td>The President, Treasurer, and General Secretary shall form the executive of the society.</td>
</tr>
<tr valign="top">
<td>4.5</td>
<td>The executive shall be elected annually</td>
</tr>
</tbody>
</table>
<h2>5	Committee And Officers Of The Society</h2>
<table border="0">
<tbody>
<tr valign="top">
<td>5.1</td>
<td>The committee of the society shall consist of the three executive officers (above) and any other officers that may be necessary.</td>
</tr>
<tr valign="top">
<td>5.2</td>
<td>The committee can decide to allocate whatever duties and privileges that it sees fit to society members.</td>
</tr>
</tbody>
</table>
<h2>6	Elections &amp; Co-opting Of Officers of the Society</h2>
<table border="0">
<tbody>
<tr valign="top">
<td>6.1</td>
<td>Any member can be co-opted by the committee to any non-executive post of the society. Any such post shall be subject to ratification at the next members' meeting, unless the post is a post requiring technical competence; in which case no ratification is required.</td>
</tr>
<tr valign="top">
<td>6.2</td>
<td>The executive have the right of veto over the election of an officer to a technical post, if the executive believe the elected officer does not possess the competence or would not have the confidence of the executive.</td>
</tr>
<tr valign="top">
<td>6.3</td>
<td>The returning officer has a duty to make a reasonable attempt to avoid an incidence of 6.2 by informing a prospective candidate (who would cause an incidence of 6.2) of clause 6.2 and its consequences.</td>
</tr>
</tbody>
</table>
<h3>6.4	Election Rules</h3>
<table border="0">
<tbody>
<tr>
<th>6.4.1</th><th align="left">Returning Officer</th>
</tr>
<tr valign="top">
<td>(i)</td>
<td>The returning officer MUST NOT be any person standing for election or re-election.</td>
</tr>
<tr valign="top">
<td>(ii)</td>
<td>The returning officer shall be the General Secretary or one of their deputies unless (i) disallows this, in which case a returning officer shall be appointed by the committee.</td>
</tr>
<tr valign="top">
<td>(iii)</td>
<td>The returning officer MUST at all times act with impartiality and must not publicly declare a preference for any candidate.</td>
</tr>
<tr valign="top">
<td>(iv)</td>
<td>The returning officer shall ensure fair play in any election; they shall have the duty of enforcing the election rules and their decisions are final.</td>
</tr>
<tr valign="top">
<td>(v)</td>
<td>The returning officer may at their discretion disqualify a candidate or call another election in the event of breach of rules or activities that may make the election unfair.</td>
</tr>
<tr valign="top">
<td>(vi)</td>
<td>Complaints against the returning officer must be made to the committee who may call a new election or disregard the complaint.</td>
</tr>
<tr>
<th>6.4.2</th><th align="left">Election Procedure</th>
</tr>
<tr valign="top">
<td>(i)</td>
<td>Each candidate for election must have a proposer and a seconder, neither of whom may be the candidate themselves.</td>
</tr>
<tr valign="top">
<td>(ii)</td>
<td>Both proposer and seconder must be student members at the time nominations for elections open, and must have been student members for a period of at least 14 consecutive days before nominations opened.</td>
</tr>
<tr valign="top">
<td>(iii)</td>
<td>Candidates for committee posts must be student members at the time nominations for elections open, and must have been student members for a period of at least 14 consecutive days before nominations opened.</td>
</tr>
<tr valign="top">
<td>(iv)</td>
<td>For the purposes of the election, Student members who have been a member for a period of at least 14 consecutive days prior to the elections opening are entitled to vote.</td>
</tr>
<tr valign="top">
<td>(v)</td>
<td>Candidates for committee posts must have a reasonable expectation of being eligible for student membership of the Society for the entire duration of their term, should they be elected. In cases of doubt, the decision is at the sole discretion of the Returning Officer.</td>
</tr>
<tr valign="top">
<td>(vi)</td>
<td>Each candidate may oversee (or nominate a representative to oversee) the counting of votes cast in an election in which they are a candidate.</td>
</tr>
<tr valign="top">
<td>(vii)</td>
<td>A member unable to vote at the required time may cast their vote by naming their preferred candidate in a written note to the returning officer which must be signed by the voter in the presence of the returning officer.</td>
</tr>
<tr valign="top">
<td>(viii)</td>
<td>There shall be at least 2 days between close of nominations and the start of the election.</td>
</tr>
<tr valign="top">
<td>(ix)</td>
<td>Nominations may be received by the returning officer immediately after the elections are announced (this is the opening date) up to and including the closing date for nominations. The closing date for nominations cannot be less than 7 days after the opening date for nominations.</td>
</tr>
</tbody>
</table>
<h2>7   Constitutional changes</h2>
<table border="0">
<tbody>
<tr valign="top">
<td>7.1</td>
<td>Changes to the constitution must be approved by at least 2/3rds of those present at a quorate meeting called in accordance with section 3.1</td>
</tr>
</tbody>
</table>