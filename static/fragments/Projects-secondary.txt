<div class="cbb">
<h3>Get involved!</h3>
<p>If you&#39;re interested in any of the SUCS projects, why not get involved by joining the <a href="http://lists.sucs.org/mailman/listinfo/devel">SUCS Development mailing list</a>?</p> 
<p>It&#39;s a great way to brush up on your skills or learn something new, with the guidance of more experienced SUCS members.</p>
</div>
